﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;

    public bool sprinting;

    public Text ghostText;
    public Text startText;

    public GameObject startPanel;
    public GameObject ghostPanel;
    

    Vector3 m_Movement;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Quaternion m_Rotation = Quaternion.identity;
   
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioSource = GetComponent<AudioSource>();
        sprinting = false;
        ghostText.text = "";
        startText.text = "";
        ghostPanel.SetActive(false);
        startPanel.SetActive(false);
    }

   
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        if (Input.GetKeyDown(KeyCode.LeftShift))
            {
            sprinting = true;
            }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            sprinting = false;
        }

        if (sprinting == true)
        {
            m_Movement.Set(2 * horizontal, 0f, 2 * vertical);
        }

        bool hasHorizontalInput = !Mathf.Approximately (horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool("IsWalking", isWalking);

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }
        

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + transform.TransformDirection(m_Movement) * m_Animator.deltaPosition.magnitude);
        //m_Rigidbody.MoveRotation(m_Rotation);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bathroom"))
        {
            ghostText.text = "Greetings, young Lemon. I am the almighty Shower Ghost! If you want to escape this house, listen carefully. Four of my brethren wander these halls and will try to stop you. Furthermore, they have placed statues around the house which will alert them to your presence. Avoid both them and my brethren at all cost, for if they catch you will awake to find all your progress loss. Only when you see the bright light at the end of a long hallway will you be able to escape!";
            ghostPanel.SetActive(true);
        }
        if (other.gameObject.CompareTag("Start"))
        {
            startText.text = "Welcome, young Lemon. Make your way through this house if you want to escape. While I recommend taking your time and moving slowly, do not forget that you can sprint by pressing the LEFT SHIFT if you feel you need to move quickly. You may also notice that you are seeing the world a bit differently, but do not worry as you are only seeing it through your own eyes. If you want to speak with me further, come to the bathroom.";
            startPanel.SetActive(true);
        }
    }
    void OnTriggerExit(Collider other)
    { 
        if (other.gameObject.CompareTag("Bathroom"))
        {
            ghostText.text = "";
            ghostPanel.SetActive(false);
            
        }
        if (other.gameObject.CompareTag("Start")) 
        {
            startText.text = "";
            startPanel.SetActive(false);
            
        }
    }

}
